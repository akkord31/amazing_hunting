from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from vacancies.models import Vacancy


def hello(request):
    return HttpResponse("Hello World!")


def vacancies(request):
    if request.method == 'GET':
        vacancies = Vacancy.objects.all()

        response = []
        for vacancy in vacancies:
            response.append({
                "id": vacancy.id,
                "text": vacancy.text
            })

        return JsonResponse(response, safe=False)
